﻿var markers = new Array();
var map, infoWindow, pos;
var table = null;
var centermarker;
function initMap() {
    // open the map
    map = new google.maps.Map(document.getElementById('map-block'), {
        zoom: 12, //default zoom
        center: { lat: -28.643387, lng: 153.612224 },//default center
        //set ctrls
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.TOP_CENTER
        },
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        scaleControl: true,
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },
        fullscreenControl: true
    });
    //set the search box
    var card = document.getElementById('pac-card');
    var input = document.getElementById('pac-input');
    //map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
    //the autocomplete tool
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.setComponentRestrictions({ 'country': ['au'] });//australia
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
    //set event listener
    autocomplete.addListener('place_changed', function () {
        //get the input place
        var place = autocomplete.getPlace();
        //error
        if (!place.geometry) {
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }
        //set selected position
        pos = {
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng()
        };
        localStorage.setItem('lat', pos.lat);
        localStorage.setItem('lng', pos.lng);
        localStorage.setItem('time', new Date().getTime());
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(pos);
            //map.setZoom(13);
        }
        map.setCenter(pos);
        //generate the table of hobby classes
        generateTable(pos);
        //set center marker
        if (centermarker !== null) {
            centermarker.setMap(null);
        }
        centermarker = new google.maps.Marker({
            position: pos,
            map: map,
            title: 'You are here!'
        });
        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }
    });
    var tmplat = parseFloat(localStorage.getItem('lat'));
    var tmplng = parseFloat(localStorage.getItem('lng'));
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            //get current location
            pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            if ((pos.lat === tmplat && pos.lng === tmplng) || (isNaN(tmplat) && isNaN(tmplng))
                || new Date().getTime() - localStorage.getItem('time') > 300000) {
                localStorage.setItem('lat', pos.lat);
                localStorage.setItem('lng', pos.lng);
                localStorage.setItem('time', new Date().getTime());
                map.setCenter(pos);
                generateTable(pos);

                centermarker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    title: 'You are here!'
                });
            }
            else {
                pos = {
                    lat: tmplat,
                    lng: tmplng
                };
                map.setCenter(pos);
                generateTable(pos);

                centermarker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    title: 'You are here!'
                });
            }
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}