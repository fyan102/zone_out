﻿var ctx = document.getElementById('canvasOutput').getContext("2d");
var cPushArray = new Array();
var cStep = -1;
var currentColorNumber = 0;
var currentColor = null;
var colorNumber = new Array();
var colorNumberRecycle = new Array();
function renew() {
    cStep = -1;
    cPushArray = new Array();
    colorNumber = new Array();
    colorNumberRecycle = new Array();
}
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('canvasOutput').toDataURL());
    //document.title = cStep + ":" + cPushArray.length;
}
function cUndo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); };
        //document.title = cStep + ":" + cPushArray.length;
        var tmp = colorNumber.pop();
        colorNumberRecycle.push(tmp);
    }
    if (cStep <= 0) {
        $('#btn-undo').addClass('disabled');
    } else {
        $('#btn-undo').removeClass('disabled');
    }
}
function cRedo() {
    if (cStep < cPushArray.length - 1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); };
        var tmp = colorNumberRecycle.pop();
        colorNumber.push(tmp);
        //document.title = cStep + ":" + cPushArray.length;
    }
    if (cStep >= cPushArray.length - 1) {
        $('#btn-redo').addClass('disabled');
    } else {
        $('#btn-redo').removeClass('disabled');
    }
}
function downloadImg() {
    button = document.getElementById('btn-download');
    canvas = document.getElementById('canvasOutput');
    var dataURL = canvas.toDataURL('image/png');
    button.href = dataURL;
}
function reset() {
    var canvasPic = new Image();
    canvasPic.src = cPushArray[0];
    canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); };
    //document.title = cStep + ":" + cPushArray.length;
    renew();
}

function drawBarChart(emotions) {
    $('svg').empty();
    const svg = d3.select('svg');
    const svgContainer = d3.select('#chart-container');
    var sum = 0;
    var max = 0;
    emotions.forEach(function (x) {
        sum += x.ratio;
        if (x.ratio > max) {
            max = x.ratio;
        }
    });
    sum = sum / 100;
    

    /*const margin = 40;
    const width = window.screen.availWidth * 0.5 - 2 * margin;
    const height = window.screen.availHeight*0.6 - 2 * margin;

    const chart = svg.append('g')
        .attr('transform', `translate(${margin}, ${margin})`);

    const xScale = d3.scaleBand()
        .range([0, width])
        .domain(emotions.map((s) => s.emotion))
        .padding(0.5);

    const yScale = d3.scaleLinear()
        .range([height, 0])
        .domain([0, max*1.1/sum]);

    const makeYLines = () => d3.axisLeft()
        .scale(yScale);

    chart.append('g')
        .attr('transform', `translate(0, ${height})`)
        .call(d3.axisBottom(xScale));

    chart.append('g')
        .call(d3.axisLeft(yScale));

    chart.append('g')
        .attr('class', 'grid')
        .call(makeYLines()
            .tickSize(-width, 0, 0)
            .tickFormat('')
        );

    const barGroups = chart.selectAll()
        .data(emotions)
        .enter()
        .append('g');

    barGroups
        .append('rect')
        .attr('class', 'bar')
        .attr('x', (g) => xScale(g.emotion))
        .attr('y', (g) => yScale(g.ratio/sum))
        .attr('height', (g) => height - yScale(g.ratio/sum))
        .attr('width', xScale.bandwidth())
        .on('mouseenter', function (actual, i) {
            d3.selectAll('.ratio')
                .attr('opacity', 0);
            d3.select(this)
                .transition()
                .duration(300)
                .attr('opacity', 0.6)
                .attr('x', (a) => xScale(a.emotion) - 5)
                .attr('width', xScale.bandwidth() + 10);

            const y = yScale(actual.ratio/sum);

            line = chart.append('line')
                .attr('id', 'limit')
                .attr('x1', 0)
                .attr('y1', y)
                .attr('x2', width)
                .attr('y2', y);

            barGroups.append('text')
                .attr('class', 'divergence')
                .attr('x', (a) => xScale(a.emotion) + xScale.bandwidth() / 2)
                .attr('y', (a) => yScale(a.ratio/sum) + 30)
                .attr('fill', 'white')
                .attr('text-anchor', 'middle')
                .text((a, idx) => {
                    const divergence = (a.ratio/sum - actual.ratio/sum).toFixed(1);
                    let text = '';
                    if (divergence > 0) text += '+';
                    text += `${divergence}%`;
                    return idx !== i ? text : '';
                });
        })
        .on('mouseleave', function () {
            d3.selectAll('.ratio')
                .attr('opacity', 1);
            d3.select(this)
                .transition()
                .duration(300)
                .attr('opacity', 1)
                .attr('x', (a) => xScale(a.emotion))
                .attr('width', xScale.bandwidth());
            chart.selectAll('#limit').remove();
            chart.selectAll('.divergence').remove();
        });

    barGroups
        .append('text')
        .attr('class', 'ratio')
        .attr('x', (a) => xScale(a.emotion) + xScale.bandwidth() / 2)
        .attr('y', (a) => yScale(a.ratio/sum) + 30)
        .attr('text-anchor', 'middle')
        .text((a) => `${(a.ratio / sum).toFixed(1)}%`);

    svg
        .append('text')
        .attr('class', 'label')
        .attr('x', -(height / 2) - margin)
        .attr('y', margin / 2.4)
        .attr('transform', 'rotate(-90)')
        .attr('text-anchor', 'middle')
        .text('Ratio (%)');

    svg.append('text')
        .attr('class', 'label')
        .attr('x', width / 2 + margin)
        .attr('y', height + margin * 1.7)
        .attr('text-anchor', 'middle')
        .text('Emotions');

    svg.append('text')
        .attr('class', 'title')
        .attr('x', width / 2 + margin)
        .attr('y', 40)
        .attr('text-anchor', 'middle')
        .text('The Ratio of Emotions');*/
}