﻿function analyze() {
    var colorNumberSum = new Array();
    var emotions = new Array();
    colorNumber.forEach(function (x) {
        try {
            var tmp = colorNumberSum.filter(y => {
                return y.color === x.color;
            });
            if (tmp.length === 0) {
                colorNumberSum.push({ 'color': x.color, 'sum': x.pix });
            } else {
                tmp[0].sum += x.pix;
            }
        } catch{
            colorNumberSum.push(x);
        }
    });
    colorNumberSum.forEach(function (x) {
        var xmlhttp = new XMLHttpRequest();
        var url = '/api/Colors/Emotion/' + x.color.split('#')[1];
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                emotionJson = JSON.parse(this.responseText);
                emotionJson.Color_Emotion.forEach(function (em) {
                    var tmp = emotions.filter(y => {
                        return y.emotion === em.Emotion.EmotionName;
                    });
                    if (tmp.length === 0) {
                        emotions.push({
                            'emotion': em.Emotion.EmotionName,
                            'desc': em.Emotion.Description,
                            'ratio': em.Probability * x.sum
                        });
                    }
                    else {
                        tmp[0].ratio += em.Probability * x.sum;
                    }
                });
            }
        };
        xmlhttp.open('GET', url, true);
        xmlhttp.send();
    });
    setTimeout(function () {
        emotions = emotions.sort(function (e1, e2) {
            return e2.ratio - e1.ratio;
        });
        console.log(emotions);
        document.getElementById('modal-body').innerHTML =
            '<h5>Your emotion now is most likely ' + emotions[0].emotion +
            '</h5><p>' + emotions[0].desc + '</p>';
        drawBarChart(emotions);
    }, 1000);
}