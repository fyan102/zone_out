﻿$(function () {
    if (window.screen.availWidth > 800) {
        $('#map-block').css({
            'left': 0,
            'height': window.screen.availHeight * 0.6,
            'width': $('.row').width() * 0.4
        });
    } else {
        $('#map-block').css({
            'left': 0,
            'height': window.screen.availHeight * 0.6,
            'width': window.screen.availWidth * 0.9
        });
    }
});



dataTb = function () {
    table = $('#data').DataTable({
        "order": [[3, 'asc']],
        "sPaginationType": "full_numbers",
        "sDom": '<"nav">t<"nav"ip>',
        "pageLength": 5
    });

    //for every row in data table, show the markers
    table.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var rowNode = this.node();
        $(rowNode).find('td:visible').each(function () {
            var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
            //markers
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(placeArr[rowIdx].Latitude,
                    placeArr[rowIdx].Longitude),
                map: map,
                icon: image,
                map_icon_label: '<span class="map-icon map-icon-jewelry-store"></span>',
                title: placeArr[rowIdx].Name,
                animation: google.maps.Animation.DROP
            });
            //add listener for markers
            marker.addListener('click', function () {
                if ($('#tr-' + rowIdx).hasClass('dataHover')) {
                    $('#tr-' + rowIdx).removeClass('dataHover');
                } else {
                    $('#tr-' + rowIdx).addClass('dataHover');
                    for (var i = 0; i < 5; i++) {
                        if (Math.floor(rowIdx / 5) * 5 + i !== rowIdx) {
                            $('#tr-' + (Math.floor(rowIdx / 5) * 5 + i)).removeClass('dataHover');
                        }
                    }
                }
                infoWindow = new google.maps.InfoWindow({
                    content: '<h6>' + placeArr[rowIdx].Organisation + '</h6>' +
                        '<p>' + placeArr[rowIdx].Address + ',' +
                        placeArr[rowIdx].Suburb + '<p>' +
                        '<a class="btn btn-small" href=\'/Places/Details/'
                        + placeArr[rowIdx].Id + '\'>Detail</a>'
                });
                infoWindow.open(map, marker);
            });
            markers.push(marker);
        });
    });
    //table draw event listener
    table.on('draw', function () {
        while (markers.length > 0) {
            var m = markers.pop();
            m.setMap(null);
        }

        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var rowNode = this.node();
            $(rowNode).find('td:visible').each(function () {
                var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(placeArr[rowIdx].Latitude,
                        placeArr[rowIdx].Longitude),
                    map: map,
                    icon: image,
                    title: placeArr[rowIdx].Organisation,
                    animation: google.maps.Animation.DROP
                });
                marker.addListener('click', function () {
                    if ($('#tr-' + rowIdx).hasClass('dataHover')) {
                        $('#tr-' + rowIdx).removeClass('dataHover');
                    } else {
                        $('#tr-' + rowIdx).addClass('dataHover');
                        for (var i = 0; i < 5; i++) {
                            if (Math.floor(rowIdx / 5) * 5 + i !== rowIdx) {
                                $('#tr-' + (Math.floor(rowIdx / 5) * 5 + i)).removeClass('dataHover');
                            }
                        }
                    }
                    infoWindow = new google.maps.InfoWindow({
                        content: '<h6>' + placeArr[rowIdx].Organisation + '</h6>' +
                            '<p>' + placeArr[rowIdx].Address + ',' +
                            placeArr[rowIdx].Suburb + '<p>' +
                            '<a class="btn btn-sm btn-primary" href=\'/Places/Details/'
                            + placeArr[rowIdx].Id + '\'>Detail</a>'
                    });
                    infoWindow.open(map, marker);
                });
                markers.push(marker);
            });
        });
        $('table tbody tr').mouseover(function () {
            $(this).addClass('dataHover');
        });
        $('table tbody tr').mouseout(function () {
            $(this).removeClass('dataHover');
        });
    });
    $('table tbody tr').mouseover(function () {
        $(this).addClass('dataHover');
    });
    $('table tbody tr').mouseout(function () {
        $(this).removeClass('dataHover');
    });
    
    $('label').css({
        'font-weight': 'bold',
        'color': 'black',
        'font-size': 'medium'
    });
};