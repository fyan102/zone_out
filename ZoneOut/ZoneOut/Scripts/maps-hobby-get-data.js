﻿var hobbyClassArr;
function generateTable(p) {
    var xmlhttp = new XMLHttpRequest();
    var url = "/api/HobbyClassApi/GetHobbyClassByLocationAsync?lng=" + p.lat + '&lat=' + p.lng;
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            hobbyClassArr = JSON.parse(this.responseText);
            getHobbyClasses(hobbyClassArr);
        }
    };
    xmlhttp.open('GET', url, true);
    xmlhttp.send();
    function getHobbyClasses(arr) {
        var out = "";
        var i;
        // destroy DataTable
        if (table !== null) {
            table.destroy();
        }
        for (i = 0; i < arr.length; i++) {
            out += '<tr id=\'tr-' + i + '\'><td><h6 style="font-weight:bold">' +
                arr[i].Name + '</h6></td><td style="float:right;font-weight:bold">' +
                arr[i].Category + '</td><td>' + arr[i].Address + ', ' +
                arr[i].Suburb + ', ' + arr[i].Postcode + ', ' +
                arr[i].State + '</td><td style="float:right">' +
                distance(p.lat, p.lng, arr[i].longtitude, arr[i].latitude)
                + 'km</td><td><a class=\'btn btn-small\' href=\'/HobbyClass/Details/'
                + arr[i].Id + '\'>Detail</a></td></tr>';
        }
        document.getElementById('table-body').innerHTML = out;
        dataTb();
    }
    function distance(lat1, lng1, lat2, lng2) {
        C = Math.sin((90 + lat1) / 180 * Math.PI) * Math.sin((90 + lat2) / 180 * Math.PI) *
            Math.cos((lng1 - lng2) / 180 * Math.PI) +
            Math.cos((90 + lat1) / 180 * Math.PI) * Math.cos((90 + lat2) / 180 * Math.PI);
        return (6378.14 * Math.acos(C)).toFixed(1);
    }
}