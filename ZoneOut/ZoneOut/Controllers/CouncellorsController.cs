﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ZoneOut.Models;

namespace ZoneOut.Controllers
{
    public class CouncellorsController : Controller
    {
        private ZoneOutDatabaseEntities db = new ZoneOutDatabaseEntities();

        // GET: Councellors
        public async Task<ActionResult> Index()
        {
            return View(await db.Councellors.ToListAsync());
        }

        // GET: Councellors/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Councellor councellor = await db.Councellors.FindAsync(id);
            if (councellor == null)
            {
                return HttpNotFound();
            }
            return View(councellor);
        }
        /*
        // GET: Councellors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Councellors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Address,Suburb,State,Postcode,Phone,Details,Latitude,Longitude")] Councellor councellor)
        {
            if (ModelState.IsValid)
            {
                db.Councellors.Add(councellor);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(councellor);
        }

        // GET: Councellors/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Councellor councellor = await db.Councellors.FindAsync(id);
            if (councellor == null)
            {
                return HttpNotFound();
            }
            return View(councellor);
        }

        // POST: Councellors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Address,Suburb,State,Postcode,Phone,Details,Latitude,Longitude")] Councellor councellor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(councellor).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(councellor);
        }

        // GET: Councellors/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Councellor councellor = await db.Councellors.FindAsync(id);
            if (councellor == null)
            {
                return HttpNotFound();
            }
            return View(councellor);
        }

        // POST: Councellors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Councellor councellor = await db.Councellors.FindAsync(id);
            db.Councellors.Remove(councellor);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }*/

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
