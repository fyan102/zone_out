﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ZoneOut.Models;

namespace ZoneOut.Controllers
{
    public class PlacesApiController : ApiController
    {
        private ZoneOutDatabaseEntities db = new ZoneOutDatabaseEntities();

        // GET: api/PlacesApi
        public IQueryable<Place> GetPlaces()
        {
            return db.Places;
        }

        // GET: api/PlacesApi/5
        [ResponseType(typeof(Place))]
        public async Task<IHttpActionResult> GetPlace(int id)
        {
            Place place = await db.Places.FindAsync(id);
            if (place == null)
            {
                return NotFound();
            }

            return Ok(place);
        }
        [ResponseType(typeof(List<Place>))]
        public IHttpActionResult GetPlacesByLocationAsync(double lng, double lat)
        {
            List<Place> hobby_Classes = db.Places.Where(x =>
            lat - 0.2 < x.Latitude && lat + 0.2 > x.Latitude &&
            lng - 0.2 < x.Longitude && lng + 0.2 > x.Longitude)
            .ToList();
            hobby_Classes.Sort(delegate (Place x, Place y)
            {
                var dist1 = (x.Latitude - lat) * (x.Latitude - lat) + (x.Longitude - lng) * (x.Longitude - lng);
                var dist2 = (y.Latitude - lat) * (y.Latitude - lat) + (y.Longitude - lng) * (y.Longitude - lng);
                if (dist1 == dist2) return 0;
                else if (dist1 > dist2) return 1;
                else return -1;
            });
            if (hobby_Classes == null)
            {
                return NotFound();
            }

            return Ok(hobby_Classes);
        }
        /*
        // PUT: api/PlacesApi/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPlace(int id, Place place)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != place.Id)
            {
                return BadRequest();
            }

            db.Entry(place).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlaceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PlacesApi
        [ResponseType(typeof(Place))]
        public async Task<IHttpActionResult> PostPlace(Place place)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Places.Add(place);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PlaceExists(place.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = place.Id }, place);
        }

        // DELETE: api/PlacesApi/5
        [ResponseType(typeof(Place))]
        public async Task<IHttpActionResult> DeletePlace(int id)
        {
            Place place = await db.Places.FindAsync(id);
            if (place == null)
            {
                return NotFound();
            }

            db.Places.Remove(place);
            await db.SaveChangesAsync();

            return Ok(place);
        }*/

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PlaceExists(int id)
        {
            return db.Places.Count(e => e.Id == id) > 0;
        }
    }
}