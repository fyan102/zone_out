﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ZoneOut.Models;

namespace ZoneOut.Controllers
{
    public class HobbyClassController : Controller
    {
        private ZoneOutDatabaseEntities db = new ZoneOutDatabaseEntities();

        // GET: HobbyClass
        public async Task<ActionResult> Index()
        {
            return View(await db.Hobby_Class.ToListAsync());
        }

        // GET: HobbyClass/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hobby_Class hobby_Class = await db.Hobby_Class.FindAsync(id);
            if (hobby_Class == null)
            {
                return HttpNotFound();
            }
            return View(hobby_Class);
        }
        /*
        // GET: HobbyClass/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HobbyClass/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Address,Suburb,Postcode,State,Category,LGA,Region")] Hobby_Class hobby_Class)
        {
            if (ModelState.IsValid)
            {
                db.Hobby_Class.Add(hobby_Class);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(hobby_Class);
        }

        // GET: HobbyClass/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hobby_Class hobby_Class = await db.Hobby_Class.FindAsync(id);
            if (hobby_Class == null)
            {
                return HttpNotFound();
            }
            return View(hobby_Class);
        }

        // POST: HobbyClass/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Address,Suburb,Postcode,State,Category,LGA,Region")] Hobby_Class hobby_Class)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hobby_Class).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(hobby_Class);
        }

        // GET: HobbyClass/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hobby_Class hobby_Class = await db.Hobby_Class.FindAsync(id);
            if (hobby_Class == null)
            {
                return HttpNotFound();
            }
            return View(hobby_Class);
        }

        // POST: HobbyClass/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Hobby_Class hobby_Class = await db.Hobby_Class.FindAsync(id);
            db.Hobby_Class.Remove(hobby_Class);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }*/

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
