﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ZoneOut.Models;

namespace ZoneOut.Controllers
{
    public class HobbyClassApiController : ApiController
    {
        private ZoneOutDatabaseEntities db = new ZoneOutDatabaseEntities();

        // GET: api/HobbyClassApi
        public IQueryable<Hobby_Class> GetHobby_Class()
        {
            return db.Hobby_Class;
        }

        // GET: api/HobbyClassApi/5
        [ResponseType(typeof(Hobby_Class))]
        public async Task<IHttpActionResult> GetHobby_Class(int id)
        {
            Hobby_Class hobby_Class = await db.Hobby_Class.FindAsync(id);
            if (hobby_Class == null)
            {
                return NotFound();
            }

            return Ok(hobby_Class);
        }

        [ResponseType(typeof(List<Hobby_Class>))]
        public IHttpActionResult GetHobbyClassByLocationAsync(double lng, double lat)
        {
            List<Hobby_Class> hobby_Classes = db.Hobby_Class.Where(x =>
            lat - 0.05 < x.latitude && lat + 0.05 > x.latitude &&
            lng - 0.05 < x.longtitude && lng + 0.05 > x.longtitude)
            .ToList();
            hobby_Classes.Sort(delegate (Hobby_Class x, Hobby_Class y)
            {
                var dist1 = (x.latitude - lat) * (x.latitude - lat) + (x.longtitude - lng) * (x.longtitude - lng);
                var dist2 = (y.latitude - lat) * (y.latitude - lat) + (y.longtitude - lng) * (y.longtitude - lng);
                if (dist1 == dist2) return 0;
                else if (dist1 > dist2) return 1;
                else return -1;
            });
            if (hobby_Classes == null)
            {
                return NotFound();
            }

            return Ok(hobby_Classes);
        }
        /*
        // PUT: api/HobbyClassApi/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutHobby_Class(int id, Hobby_Class hobby_Class)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hobby_Class.Id)
            {
                return BadRequest();
            }

            db.Entry(hobby_Class).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Hobby_ClassExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/HobbyClassApi
        [ResponseType(typeof(Hobby_Class))]
        public async Task<IHttpActionResult> PostHobby_Class(Hobby_Class hobby_Class)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Hobby_Class.Add(hobby_Class);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = hobby_Class.Id }, hobby_Class);
        }

        // DELETE: api/HobbyClassApi/5
        [ResponseType(typeof(Hobby_Class))]
        public async Task<IHttpActionResult> DeleteHobby_Class(int id)
        {
            Hobby_Class hobby_Class = await db.Hobby_Class.FindAsync(id);
            if (hobby_Class == null)
            {
                return NotFound();
            }

            db.Hobby_Class.Remove(hobby_Class);
            await db.SaveChangesAsync();

            return Ok(hobby_Class);
        }*/

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Hobby_ClassExists(int id)
        {
            return db.Hobby_Class.Count(e => e.Id == id) > 0;
        }
    }
}