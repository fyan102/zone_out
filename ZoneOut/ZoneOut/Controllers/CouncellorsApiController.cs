﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ZoneOut.Models;

namespace ZoneOut.Controllers
{
    public class CouncellorsApiController : ApiController
    {
        private ZoneOutDatabaseEntities db = new ZoneOutDatabaseEntities();

        // GET: api/CouncellorsApi
        public IQueryable<Councellor> GetCouncellors()
        {
            return db.Councellors;
        }

        // GET: api/CouncellorsApi/5
        [ResponseType(typeof(Councellor))]
        public async Task<IHttpActionResult> GetCouncellor(int id)
        {
            Councellor councellor = await db.Councellors.FindAsync(id);
            if (councellor == null)
            {
                return NotFound();
            }

            return Ok(councellor);
        }
        [ResponseType(typeof(List<Councellor>))]
        public IHttpActionResult GetCouncellorsByLocationAsync(double lng, double lat)
        {
            List<Councellor> hobby_Classes = db.Councellors.Where(x =>
            lat - 0.2 < x.Latitude && lat + 0.2 > x.Latitude &&
            lng - 0.2 < x.Longitude && lng + 0.2 > x.Longitude)
            .ToList();
            hobby_Classes.Sort(delegate (Councellor x, Councellor y)
            {
                var dist1 = (x.Latitude - lat) * (x.Latitude - lat) + (x.Longitude - lng) * (x.Longitude - lng);
                var dist2 = (y.Latitude - lat) * (y.Latitude - lat) + (y.Longitude - lng) * (y.Longitude - lng);
                if (dist1 == dist2) return 0;
                else if (dist1 > dist2) return 1;
                else return -1;
            });
            if (hobby_Classes == null)
            {
                return NotFound();
            }

            return Ok(hobby_Classes);
        }
        /*
        // PUT: api/CouncellorsApi/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCouncellor(int id, Councellor councellor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != councellor.Id)
            {
                return BadRequest();
            }

            db.Entry(councellor).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CouncellorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CouncellorsApi
        [ResponseType(typeof(Councellor))]
        public async Task<IHttpActionResult> PostCouncellor(Councellor councellor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Councellors.Add(councellor);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = councellor.Id }, councellor);
        }

        // DELETE: api/CouncellorsApi/5
        [ResponseType(typeof(Councellor))]
        public async Task<IHttpActionResult> DeleteCouncellor(int id)
        {
            Councellor councellor = await db.Councellors.FindAsync(id);
            if (councellor == null)
            {
                return NotFound();
            }

            db.Councellors.Remove(councellor);
            await db.SaveChangesAsync();

            return Ok(councellor);
        }
        */
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CouncellorExists(int id)
        {
            return db.Councellors.Count(e => e.Id == id) > 0;
        }
    }
}