﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using ZoneOut.Models;

namespace ZoneOut.Controllers
{
    
    public class HomeController : Controller
    {
        private readonly double COOKIE_EXPIRES = 30;

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Infinite Loops";
            return View();
        }
        public ActionResult Statistics()
        {
            ViewBag.Message = "How can art (and other activities) help me?";
            return View();
        }
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (model.UserName.Equals("zoneout") && model.Password.Equals("infinite081Loop"))
            {
                string userData = new JavaScriptSerializer().Serialize(model);
                var ticket = new FormsAuthenticationTicket(1, model.UserName, DateTime.Now, DateTime.Now.AddDays(COOKIE_EXPIRES), false, userData, FormsAuthentication.FormsCookiePath);
                string encrypt = FormsAuthentication.Encrypt(ticket);
                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypt);
                if (model.RememberMe)
                {
                    cookie.Expires = DateTime.Now.AddDays(COOKIE_EXPIRES);
                }
                Response.Cookies.Remove(cookie.Name);
                Response.Cookies.Add(cookie);

                if (string.IsNullOrEmpty(returnUrl))
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                    return Redirect(returnUrl);
            }
            else
            {
                ModelState.AddModelError("", "Invalid Login");
                return View(model);
            }
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return Redirect(FormsAuthentication.LoginUrl);
        }
        public ActionResult Quiz()
        {
            return View();
        }
        public ActionResult Error(String aspxerrorpath)
        {
            return View();
        }
    }
}