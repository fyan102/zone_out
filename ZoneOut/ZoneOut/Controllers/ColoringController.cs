﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZoneOut.Models;

namespace ZoneOut.Controllers
{
    public class ColoringController : Controller
    {
        private readonly int NumberOfPictures = 24;
        // GET: Coloring
        public ActionResult Index()
        {
            List<Coloring> Colorings = new List<Coloring>();
            for(int i=0; i < NumberOfPictures; i++)
            {
                Colorings.Add(new Coloring { Id = i ,Url="/Canvas/image"+i+".jpg"});
            }
            return View(Colorings);
        }
        
        public ActionResult ColoringGameInternalRandom()
        {
            ViewBag.Message = "Coloring Game";
            var imgId = new Random().Next(NumberOfPictures);
            var Picture = new Coloring { Id = imgId, Url = "/Canvas/image" + imgId + ".jpg" };
            return View(Picture);
        }
        public ActionResult ColoringGameInternalSelected(int picId)
        {
            var Picture = new Coloring { Id = picId,Url="/Canvas/image"+picId+".jpg" };
            return View(Picture);
        }
        public ActionResult ColoringGameExternal()
        {
            return View();
        }
    }
}