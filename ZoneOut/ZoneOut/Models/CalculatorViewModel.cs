﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ZoneOut.Models
{
    public class CalculatorViewModel
    {
        [Required]
        [Display(Name = "Number of hours of sleep each night")]
        [Range(0,23)]
        public double Sleep { get; set; }
        [Required]
        [Display(Name = "Number of hours spent grooming each day")]
        [Range(0, 23)]
        public double Grooming { get; set; }
        [Required]
        [Display(Name = "Number of hours for meals/snacks (including preparation/clean-up time) each day")]
        [Range(0, 23)]
        public double Meals { get; set; }
        [Required]
        [Display(Name = "Travel time to and from campus each day")]
        [Range(0, 23)]
        public double Travel { get; set; }
        [Required]
        [Range(0, 48)]
        [Display(Name = "Number of hours for regular activities (volunteer work, doubleramurals, church, clubs, etc.) per week")]
        public double Activities { get; set; }
        [Required]
        [Range(0, 23)]
        [Display(Name = "Number of hours of errands, etc. per day")]
        public double Errands { get; set; }
        [Required]
        [Range(0, 48)]
        [Display(Name = "Number of hours of part time work per week")]
        public double Work { get; set; }
        [Required]
        [Range(0, 48)]
        [Display(Name = "Number of hours of class per week")]
        public double Class { get; set; }
        [Required]
        [Range(0, 48)]
        [Display(Name = "Number of hours with friends,  social parties, going out, etc. per week")]
        public double Friends { get; set; }
        [Required]
        [Range(0, 23)]
        [Display(Name = "Number of hours of TV and computer per day")]
        public double Computer { get; set; }
        public double Result { get; set; }
    }
    
}